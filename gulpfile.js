// -------------------------------------
//   Name: Modules
//
//   Description: All modules/plugins that will be used within the Gulpfile
//   to compile our WordPress theme.
// -------------------------------------
const browserSync = require( 'browser-sync' ).create();

const gulp = require( 'gulp' );
const util = require( 'gulp-util' );

// CSS related plugins
const sass = require( 'gulp-dart-sass' );
const sassGlob = require( 'gulp-sass-glob' ); // Glob multiple SCSS files.
const minifycss = require( 'gulp-uglifycss' ); // Minifies CSS files.
const autoprefixer = require( 'gulp-autoprefixer' ); // Autoprefixing magic.
const stripCssComments = require( 'gulp-strip-css-comments' ); // Removing comments from CSS.

// JS related plugins
const concat = require( 'gulp-concat' ); // Concatenates JS files.
const uglify = require( 'gulp-uglify' ); // Minifies JS files.
const babel = require( 'gulp-babel' ); // Compiles ESNext to browser compatible JS.

// IMG related plugins
const imagemin = require( 'gulp-imagemin' ); // Minify PNG, JPEG, GIF and SVG images with imagemin.

// Utitlity related plugins
const extension = require( 'file-extension' );
const rename = require( 'gulp-rename' ); // Place suffixs within file names.
const plumber = require( 'gulp-plumber' ); // Prevent pipe breaking caused by errors from gulp plugins.
const sourcemaps = require( 'gulp-sourcemaps' ); // Maps code in a compressed file (E.g. style.css) back to it’s original position in a source file (E.g. structure.scss, which was later combined with other css files to generate style.css).
const config = require( './../config.js' );

const error = e => {
  console.log( e.toString() );
  this.emit( 'end' );
};

// -------------------------------------
//   Name: Configuration
//
//   Description: Settings used throughout the Gulpfile.js, these settings do not
//   change often.
// -------------------------------------
const theme = `../app/public/wp-content/themes/${config.name}/`;
const settings = {
  PROD: util.env.prod,

  files: {
    theme: [ `${theme}**`, `${theme}**/*` ],

    img: [ `${theme}assets/images/raw/**`, `${theme}assets/images/raw/**/*` ],

    fonts: [ `${theme}assets/fonts/raw/**`, `${theme}assets/fonts/raw/**/*` ],

    js: [
      `${theme}assets/scripts/raw/**.js`,
      `${theme}assets/scripts/raw/**/*.js`,
      `${theme}library/framework/extensions/shortcodes/shortcodes/**/assets/*.js`,
      `${theme}templates/block/**/*.js`
    ],

    css: [
      `${theme}assets/styles/raw/**.scss`,
      `${theme}assets/styles/raw/**/*.scss`,
      `${theme}assets/styles/raw/**/**/*.scss`,
      `${theme}library/framework/extensions/shortcodes/shortcodes/**/assets/*.scss`,
      `${theme}templates/block/**/*.scss`
    ],

    php: [ `${theme}*.php`, `${theme}**/*.php` ]
  },

  themes: {
    theme,
    img: `${theme}assets/images`,
    fonts: `${theme}assets/webfonts`,
    js: `${theme}assets/scripts`,
    css: `${theme}assets/styles`
  },

  browsers: [
    'last 2 version',
    '> 1%',
    'ie >= 11',
    'last 1 Android versions',
    'last 1 ChromeAndroid versions',
    'last 2 Chrome versions',
    'last 2 Firefox versions',
    'last 2 Safari versions',
    'last 2 iOS versions',
    'last 2 Edge versions',
    'last 2 Opera versions'
  ],

  vendors: config.vendors
};

// -------------------------------------
//   Name: Styles
//
//   Description: Looking at build/sass and compiling the files into compact format
//   and autoprefixing them.
// -------------------------------------
gulp.task( 'styles', () =>
  gulp
    .src( settings.files.css )
    .pipe( sassGlob() )
    .pipe( settings.PROD ? util.noop() : sourcemaps.init() )
    .pipe(
      settings.PROD ?
        sass({
            includePaths: [ '../node_modules' ],
            outputStyle: 'compressed'
          }).on( 'error', sass.logError ) :
        sass({
            includePaths: [ '../node_modules' ],
            outputStyle: 'compressed'
          }).on( 'error', sass.logError )
    )
    .pipe( autoprefixer( 'last 2 version' ) )
    .pipe( settings.PROD ? stripCssComments({ preserve: false }) : util.noop() )
    .pipe( settings.PROD ? util.noop() : sourcemaps.write() )
    .pipe( settings.PROD ? minifycss() : util.noop() )
    .pipe( rename({ suffix: '.min' }) )
    .pipe( browserSync.stream() )
    .pipe( gulp.dest( settings.themes.css ) )
);

// -------------------------------------
//   Name: Scripts
//
//   Description: Looking at build/js and compiling the files into compact format and
//   moving them to dist folder.
// -------------------------------------
gulp.task( 'scripts', () =>
  gulp
    .src( settings.files.js )
    .pipe( plumber({ errorHandler: error }) )
    .pipe( settings.PROD ? util.noop() : sourcemaps.init() )
    .pipe( babel({ presets: [ '@babel/env' ] }) )
    .pipe( uglify() )
    .pipe( concat( 'scripts.js' ) )
    .pipe( settings.PROD ? util.noop() : sourcemaps.write() )
    .pipe( rename({ suffix: '.min' }) )
    .pipe( gulp.dest( settings.themes.js ) )
);

// -------------------------------------
//   Name: Fonts
//
//   Description: Moving vendor fonts installed bij NPM to the theme fonts library
//   such as Font Awesome.
// -------------------------------------
gulp.task( 'fonts', () =>
  gulp.src( settings.vendors.fonts ).pipe( gulp.dest( settings.themes.fonts ) )
);

// -------------------------------------
//   Name: Images
//
//   Description: Looking at build/image and compiling the files into smaller files
//   and replacing them.
// -------------------------------------
gulp.task( 'images', () =>
  gulp
    .src( settings.files.img )
    .pipe( plumber({ errorHandler: error }) )
    .pipe(
      imagemin(
        [
          imagemin.gifsicle({ interlaced: true }),
          imagemin.optipng({ optimizationLevel: 3 }),
          imagemin.svgo({ plugins: [ { removeViewBox: true } ] })
        ],
        { verbose: true }
      )
    )
    .pipe( gulp.dest( settings.themes.img ) )
);

// -------------------------------------
//   Name: Vendors
//
//   Description: Compiling all vendors script in one single file so there is
//   only 1 HTTP request required to load them.
// -------------------------------------
gulp.task( 'vendors-scripts', () =>
  gulp
    .src( settings.vendors.js )
    .pipe( plumber({ errorHandler: error }) )
    .pipe( ! settings.PROD ? sourcemaps.init() : util.noop() )
    .pipe( uglify() )
    .pipe( concat( 'vendors.min.js' ) )
    .pipe( ! settings.PROD ? sourcemaps.write() : util.noop() )
    .pipe( gulp.dest( settings.themes.js ) )
);

gulp.task( 'vendors-styles', () =>
  gulp
    .src( settings.vendors.css )
    .pipe( ! settings.PROD ? sourcemaps.init() : util.noop() )
    .pipe( plumber({ errorHandler: error }) )
    .pipe( stripCssComments({ preserve: false }) )
    .pipe( concat( 'vendors.min.css' ) )
    .pipe( ! settings.PROD ? sourcemaps.write() : util.noop() )
    .pipe( gulp.dest( settings.themes.css ) )
);

gulp.task( 'vendors', gulp.parallel( 'vendors-scripts', 'vendors-styles' ) );

// -------------------------------------
//   Name: Default
//
//   Description: Compiles styles, fires-up browser syncing and watches
//   all required files.
// -------------------------------------
// Starts a Browsersync Server and listens to changes
gulp.task( 'server', () => {
  browserSync.init({
    proxy: 'http://localhost:8080',
    logPrefix: 'Social Brothers',
    ghostMode: false,
    injectChanges: true,
    notify: false
  });

  gulp.watch( settings.files.css, { usePolling: true }, gulp.parallel( 'styles' ) );

  gulp
    .watch( settings.files.php, { usePolling: true })
    .on( 'change', browserSync.reload );
  gulp
    .watch( settings.files.js, { usePolling: true }, gulp.parallel( 'scripts' ) )
    .on( 'change', browserSync.reload );
  gulp
    .watch( settings.files.img, { usePolling: true }, gulp.parallel( 'images' ) )
    .on( 'change', browserSync.reload );

  gulp.watch( settings.files.theme, { usePolling: true }).on( 'change', file => {
    util.log(
      `${util.colors.cyan(
        `${extension( file ).toUpperCase()} changed:`
      )} ${util.colors.magenta( file )}`
    );
  });
});

// Building our whole dist or build WordPress Theme
gulp.task(
  'build',
  gulp.parallel( 'styles', 'scripts', 'images', 'fonts', 'vendors' )
);

// Building our whole dist or build WordPress Theme and starting the Browsersync Server
gulp.task( 'default', gulp.series( 'build', 'server' ) );
