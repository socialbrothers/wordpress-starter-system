include ./../.env
.DEFAULT_GOAL := help

# Variables used throughout the Makefile
USER:=`whoami`
TIMESTAMP:=`date +%s`

ORANGE=\033[1m\033[38;5;208m
GRAY=\033[38;5;242m
NC=\033[0m

# Make if not set silent by default
ifndef VERBOSE
.SILENT:
endif

# Make Windows/Linux specif prefixes
ifeq ($(OS),Windows_NT)
    PREFIX_DOCKER := winpty
	PREFIX_COLOR := -e
else
endif

# @echo ${PREFIX_COLOR} "${GRAY}Are you sure you want to do this? [y/N]:";

help:
	@make message MESSAGE="Social Brothers Functions"
	@echo "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\x1b[36m\1\\x1b[m:\2/' | column -c2 -t -s :)"

confirm:
	@echo ${PREFIX_COLOR} "${GRAY}Are you sure you want to do this? [y/N]:${NC}"
	@(read -p "" sure && case "$$sure" in [yY]) true;; *) false;; esac )

message:
	@echo "";
	@echo ${PREFIX_COLOR} "${ORANGE}${MESSAGE}${NC}"

setup: ## Setup a new project
	@make message MESSAGE="Setting up a new project and initializing a new GIT-repository for ${GIT_REMOTE_NAME}..."
	@make confirm

	@make message MESSAGE="This will remove all the GIT history of your current repository (${GIT_REMOTE_URI}) ..."
	@make confirm

	@cd ./../ && git checkout --orphan tempbranch
	@cd ./../ && git add -A
	@cd ./../ && git remote rm origin
	@cd ./../ && git remote add origin ${GIT_REMOTE_URI}
	@cd ./../ && git commit -m "Initialize"
	@cd ./../ && git branch | grep -v "tempbranch" | xargs git branch -D
	@cd ./../ && git branch -m master
	@cd ./../ && git push -u origin master
	@cd ./../ && git checkout -b develop
	@cd ./../ && git push -u origin develop
	@cd ./../ && git checkout -b feature/setup
	@cd ./../ && git push -u origin feature/setup
	@cd ./../ && git gc --aggressive --prune=all

start: ## Starts the local development envirnoment
	@lsof -i:8080 > /dev/null && make stop || echo ""

	@make message MESSAGE="Starting Docker Containers..."
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} up -d

	@make message MESSAGE="Downloading Node.js Modules..."
	@cd ./../ && yarn install

	@make message MESSAGE="Downloading Node.js Dependencies..."
	@yarn install

	@make plugins

	@make message MESSAGE="Activating WordPress plugins..."
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress wp plugin activate --all

	@make theme

	@make message MESSAGE="Starting..."
	@gulp

stop: ## Stops the local development envirnoment
	@make message MESSAGE="Stopping Docker Containers..."
	@docker container stop $$(docker container ls -aq)

plugins: ## Downloading WordPress Plugins
	@make message MESSAGE="Downloading WordPress plugins..."
	@cd ./../ && composer install --no-autoloader

wp: ## Starts the WordPress command-line-interface
	@make message MESSAGE="Starting up WordPress CLI..."
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress bash

theme: ## Set wordpress theme to socialbrothers
	@make message MESSAGE="Activating WordPress theme..."
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress wp theme activate socialbrothers


pull: ## Replacing local database with live database
	@make message MESSAGE="Replacing local database with ${MIGRATEDBPRO_URL} database..."
	@make confirm

	@make message MESSAGE="This means all your local changes will be overwritten with ${MIGRATEDBPRO_URL}..."
	@make confirm

	@make message MESSAGE="Backing up current database..."
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress bash -c "wp db export \"/data/backups/${TIMESTAMP}-${USER}.sql\" --allow-root"

	@make message MESSAGE="Pulling Database..."
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress wp migratedb setting update license $(MIGRATEDBPRO_LICENSE)

	@echo ${PREFIX_COLOR} "${GRAY}Do you also want to download the images? [y/N]:${NC}"
	@(read -p "" sure && case "$$sure" in [yY]) ${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress wp migratedb pull $(MIGRATEDBPRO_URL) $(MIGRATEDBPRO_KEY) --media=remove-and-copy;; *) ${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress wp migratedb pull $(MIGRATEDBPRO_URL) $(MIGRATEDBPRO_KEY);; esac )

	@make message MESSAGE="Setting root password back to root"
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress wp user update root --user_pass=root

push: ## Replacing live database with local database
	@make message MESSAGE="Replacing ${MIGRATEDBPRO_URL} database with local database..."
	@make confirm

	@make message MESSAGE="This means all your ${MIGRATEDBPRO_URL} changes will be overwritten with local..."
	@make confirm

	@make message MESSAGE="Darn, are you really sure? This is not common!"
	@make confirm

	@make message MESSAGE="Pushing Database..."
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress wp plugin activate wp-migrate-db-pro
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress wp plugin activate wp-migrate-db-pro-media-files
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress wp plugin activate wp-migrate-db-pro-cli
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress wp migratedb setting update license $(MIGRATEDBPRO_LICENSE)
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress wp user reset-password root
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress wp migratedb push $(MIGRATEDBPRO_URL) $(MIGRATEDBPRO_KEY) --media=compare-and-remove
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress wp user update root --user_pass=root

import: ## Replacing local database and uploads with latest export
	@make message MESSAGE="Replacing local database with latest export..."
	@make confirm

	@make message MESSAGE="Backing up current database..."
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress bash -c "wp db export \"/data/files/${TIMESTAMP}-${USER}.sql\" --allow-root"

	@make message MESSAGE="Backing up current uploads..."
	@zip -r ../data/files/${TIMESTAMP}-${USER}.zip ../build/uploads
	@rm -rfv ../build/uploads/*

	@make message MESSAGE="Importing latest export..."
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress bash -c "wp db import /data/latest.sql --allow-root"

	@make message MESSAGE="Importing latest uploads..."
	@unzip ../data/latest.zip -d ../build/uploads

export: ## Creating export of current database and uploads
	@make message MESSAGE="Creating export of current database..."

	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress bash -c "wp db export /data/latest.sql --allow-root"

	@make message MESSAGE="Creating export of current uploads..."
	@(cd ../build/uploads; zip -r ../../data/latest.zip ./*)

backup: ## Creating backup of current database and uploads
	@make message MESSAGE="Backing up current database..."
	@${PREFIX_DOCKER} docker-compose -p=${GIT_REMOTE_NAME} exec wordpress bash -c "wp db export \"/data/files/${TIMESTAMP}-${USER}.sql\" --allow-root"

	@make message MESSAGE="Backing up current uploads..."
	@zip -r ../data/files/${TIMESTAMP}-${USER}.zip ../build/uploads
